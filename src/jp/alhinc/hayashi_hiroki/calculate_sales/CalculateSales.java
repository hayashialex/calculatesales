package jp.alhinc.hayashi_hiroki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {
	public static void main(String args[]) {

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//マップを生成
		HashMap<String,String> shopMap = new HashMap<String,String>();
		//売上ファイル内部情報マップを生成
		HashMap<String, Long> salesMap = new HashMap<String,Long>();
		List<File> salesList = new ArrayList<File>();
		ArrayList<String> rcdList = new ArrayList<String>();

		if(!branchDefinition(shopMap, salesMap, salesList,  args[0], "branch.lst", "支店", "[0-9]{3}")){
			return;
		}
		if(!earningsTotal(args, salesList, rcdList, salesMap, shopMap, "^[0-9]{8}.rcd$",args[0])){
			return;
		}
		if(!writeData(shopMap, salesMap, "branch.out",args[0]))
			return;
	}
	private static boolean branchDefinition(HashMap<String,String> shopMap, HashMap<String, Long> salesMap,
			List<File> salesList,  String directyPath, String fileName, String branch, String regularNum) {
		//バッファリーダーを定義
		BufferedReader br = null;
		try {
			//"branch.lstと合致するデータを抽出
			File file = new File(directyPath, fileName);
			if (!file.exists()) {
				System.out.println(branch + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] shopData = line.split(",");
				//支店コード,支店名のマップを生成

				if(shopData.length == 2 && shopData[0].matches(regularNum)) {
					shopMap.put(shopData[0], shopData[1]);
					salesMap.put(shopData[0], 0L);
				} else {
					System.out.println(branch +"定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		}catch (IOException e){
			System.out.println("エラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	private static boolean earningsTotal(String args[], List<File> salesList, ArrayList<String> rcdList,
			HashMap<String, Long> salesMap, HashMap<String,String> shopMap, String regExpression, String directyPath){
		File dir = new File(directyPath);
		//ファイル一覧を取得
		File[] fileName = dir.listFiles();
		for(int i=0; i<fileName.length; ++i){
			//ファイル名取得＋ファイル名抽出
			if((fileName[i].getName()).matches(regExpression) && fileName[i].isFile()){
				//salesListにファイル名に対応したファイルを格納
				rcdList.add(fileName[i].getName());
				salesList.add(fileName[i]);
			}
		}

		for(int i = 0; i < salesList.size() -1; ++i){
			int o = Integer.parseInt(salesList.get(i).getName().substring(0,8));
			int n = Integer.parseInt(salesList.get(i + 1).getName().substring(0,8));
			if(n - o != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}

		// salesListを読み込み、支店コード、売上金額を抽出
		for(int i = 0; i < salesList.size(); i++) {
			BufferedReader salesBr = null;
			try {

				File salesFile = salesList.get(i);
				FileReader salesRr = new FileReader(salesFile);
				salesBr = new BufferedReader(salesRr);

				//						CodeAmountListに支店コード、売上金額を格納
				List<String> codeAmountList = new ArrayList<String>();

				String salesLine;
				while((salesLine =  salesBr.readLine()) != null) {
					String salesData = salesLine;
					codeAmountList.add(salesData);
				}
				if(codeAmountList.size() != 2) {
					System.out.println(rcdList.get(i) +"のフォーマットが不正です");
					return false;
				}
				if(!codeAmountList.get(1).matches("^[0-9]*$")) {
					System.out.println( "予期せぬエラーが発生しました");
					return false;
				}

				if(!shopMap.containsKey(codeAmountList.get(0))) {
					System.out.println(rcdList.get(i) + "の支店コードが不正です");
					return false;
				}

				//売上合計を算出
				Long num = salesMap.get(codeAmountList.get(0));
				long num2 = Long.parseLong(codeAmountList.get(1));
				Long val = num + num2;
				Long valLen = (long) String.valueOf( val ).length();
				if(valLen > 10) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				//算出した売上合計をsalesMapに格納
				salesMap.put(codeAmountList.get(0),val);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}finally{
				if(salesBr != null) {
					try {
						salesBr.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}
		return true;
	}
	private static boolean writeData(HashMap<String,String> shopMap, HashMap<String, Long> salesMap,
			String fileName, String directyPath) {
		BufferedWriter bw = null;
		try{
			File file = new File(directyPath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (Map.Entry<String, String> shopName : shopMap.entrySet()) {
				bw.write(shopName.getKey() + "," + shopName.getValue() + "," + salesMap.get(shopName.getKey()));
				bw.newLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
